package com.afs.tdd;

public class Location {
    private int coordinateX;
    private int coordinateY;
    private Direction direction;

    public Location(int x, int y, Direction direction) {
        this.coordinateX = x;
        this.coordinateY = y;
        this.direction = direction;
    }

    public int getCoordinateY() {
        return coordinateY;
    }

    public Direction getDirection() {
        return direction;
    }

    public int getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(int coordinateX) {
        this.coordinateX = coordinateX;
    }

    public void setCoordinateY(int coordinateY) {
        this.coordinateY = coordinateY;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
