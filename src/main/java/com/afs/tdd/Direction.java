package com.afs.tdd;

public enum Direction {
    SOUTH, EAST, WEST, NORTH
}
