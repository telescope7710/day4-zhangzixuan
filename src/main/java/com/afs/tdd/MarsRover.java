package com.afs.tdd;

import java.util.List;

public class MarsRover {
    private Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public Location executeCommand(Command command) {
        switch (command) {
            case MOVE:
                this.move();
                break;
            case LEFT:
                this.turnLeft();
                break;
            case RIGHT:
                this.turnRight();
                break;
        }
        return this.location;
    }

    public void move() {
        switch (this.location.getDirection()) {
            case NORTH:
                this.location.setCoordinateY(this.location.getCoordinateY() + 1);
                break;
            case SOUTH:
                this.location.setCoordinateY(this.location.getCoordinateY() - 1);
                break;
            case EAST:
                this.location.setCoordinateX(this.location.getCoordinateX() + 1);
                break;
            case WEST:
                this.location.setCoordinateX(this.location.getCoordinateX() - 1);
        }
    }

    public void turnLeft() {
        switch (this.location.getDirection()) {
            case NORTH:
                this.location.setDirection(Direction.WEST);
                break;
            case WEST:
                this.location.setDirection(Direction.SOUTH);
                break;
            case SOUTH:
                this.location.setDirection(Direction.EAST);
                break;
            case EAST:
                this.location.setDirection(Direction.NORTH);

        }

    }

    public void turnRight() {
        switch (this.location.getDirection()) {
            case NORTH:
                this.location.setDirection(Direction.EAST);
                break;
            case EAST:
                this.location.setDirection(Direction.SOUTH);
                break;
            case SOUTH:
                this.location.setDirection(Direction.WEST);
                break;
            case WEST:
                this.location.setDirection(Direction.NORTH);
        }
    }

    public Location executeBatchCommand(List<Command> commands) {
        commands.forEach(this::executeCommand);
        return location;
    }
}
