package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class MarsRoverTest {
    @Test
    void should_change_to_0_1_N_when_executeCommand_given_location_0_0_N_and_command_move() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.MOVE;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(1, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.NORTH, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_n1_S_when_executeCommand_given_location_0_0_S_and_command_move() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.SOUTH);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.MOVE;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(-1, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.SOUTH, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_1_0_E_when_executeCommand_given_location_0_0_E_and_command_move() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.EAST);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.MOVE;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(1, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.EAST, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_n1_0_W_when_executeCommand_given_location_0_0_W_and_command_move() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.WEST);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.MOVE;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(-1, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.WEST, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_W_when_executeCommand_given_location_0_0_N_and_command_left() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandLeft = Command.LEFT;
        //When
        Location locationAfterTurnLeft = marsRover.executeCommand(commandLeft);
        //Then
        Assertions.assertEquals(0, locationAfterTurnLeft.getCoordinateX());
        Assertions.assertEquals(0, locationAfterTurnLeft.getCoordinateY());
        Assertions.assertEquals(Direction.WEST, locationAfterTurnLeft.getDirection());
    }

    @Test
    void should_change_to_0_0_S_when_executeCommand_given_location_0_0_W_and_command_left() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.WEST);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandLeft = Command.LEFT;
        //When
        Location locationAfterTurnLeft = marsRover.executeCommand(commandLeft);
        //Then
        Assertions.assertEquals(0, locationAfterTurnLeft.getCoordinateX());
        Assertions.assertEquals(0, locationAfterTurnLeft.getCoordinateY());
        Assertions.assertEquals(Direction.SOUTH, locationAfterTurnLeft.getDirection());
    }

    @Test
    void should_change_to_0_0_E_when_executeCommand_given_location_0_0_S_and_command_left() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.SOUTH);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandLeft = Command.LEFT;
        //When
        Location locationAfterTurnLeft = marsRover.executeCommand(commandLeft);
        //Then
        Assertions.assertEquals(0, locationAfterTurnLeft.getCoordinateX());
        Assertions.assertEquals(0, locationAfterTurnLeft.getCoordinateY());
        Assertions.assertEquals(Direction.EAST, locationAfterTurnLeft.getDirection());
    }

    @Test
    void should_change_to_0_0_N_when_executeCommand_given_location_0_0_E_and_command_left() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.EAST);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandLeft = Command.LEFT;
        //When
        Location locationAfterTurnLeft = marsRover.executeCommand(commandLeft);
        //Then
        Assertions.assertEquals(0, locationAfterTurnLeft.getCoordinateX());
        Assertions.assertEquals(0, locationAfterTurnLeft.getCoordinateY());
        Assertions.assertEquals(Direction.NORTH, locationAfterTurnLeft.getDirection());
    }

    @Test
    void should_change_to_0_0_E_when_executeCommand_given_location_0_0_N_and_command_right() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandRight = Command.RIGHT;
        //When
        Location locationAfterTurnLeft = marsRover.executeCommand(commandRight);
        //Then
        Assertions.assertEquals(0, locationAfterTurnLeft.getCoordinateX());
        Assertions.assertEquals(0, locationAfterTurnLeft.getCoordinateY());
        Assertions.assertEquals(Direction.EAST, locationAfterTurnLeft.getDirection());
    }

    @Test
    void should_change_to_0_0_S_when_executeCommand_given_location_0_0_E_and_command_right() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.EAST);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandRight = Command.RIGHT;
        //When
        Location locationAfterTurnRight = marsRover.executeCommand(commandRight);
        //Then
        Assertions.assertEquals(0, locationAfterTurnRight.getCoordinateX());
        Assertions.assertEquals(0, locationAfterTurnRight.getCoordinateY());
        Assertions.assertEquals(Direction.SOUTH, locationAfterTurnRight.getDirection());
    }

    @Test
    void should_change_to_0_0_W_when_executeCommand_given_location_0_0_S_and_command_right() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.SOUTH);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandRight = Command.RIGHT;
        //When
        Location locationAfterTurnRight = marsRover.executeCommand(commandRight);
        //Then
        Assertions.assertEquals(0, locationAfterTurnRight.getCoordinateX());
        Assertions.assertEquals(0, locationAfterTurnRight.getCoordinateY());
        Assertions.assertEquals(Direction.WEST, locationAfterTurnRight.getDirection());
    }

    @Test
    void should_change_to_0_0_N_when_executeCommand_given_location_0_0_W_and_command_right() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.WEST);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandRight = Command.RIGHT;
        //When
        Location locationAfterTurnRight = marsRover.executeCommand(commandRight);
        //Then
        Assertions.assertEquals(0, locationAfterTurnRight.getCoordinateX());
        Assertions.assertEquals(0, locationAfterTurnRight.getCoordinateY());
        Assertions.assertEquals(Direction.NORTH, locationAfterTurnRight.getDirection());
    }

    @Test
    void should_change_to_1_1_S_when_executeBatchCommand_given_location_0_0_N_and_command_MRRLMR() {
        //Given
        Location locationInitial = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(locationInitial);
        List<Command> commands = new ArrayList<>();
        commands.add(Command.MOVE);
        commands.add(Command.RIGHT);
        commands.add(Command.RIGHT);
        commands.add(Command.LEFT);
        commands.add(Command.MOVE);
        commands.add(Command.RIGHT);
        //When
        Location locationAfterMove = marsRover.executeBatchCommand(commands);
        //Then
        Assertions.assertEquals(1, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(1, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.SOUTH, locationAfterMove.getDirection());
    }
}
