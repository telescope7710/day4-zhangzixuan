## O
- Today we had code review to enhance our ability to refactor code. 
- Then we learned about TDD(test-driven development).
- TDD has three stages, the first is to write test case, the test returns red, then realize the code is test passed, and finally refactor the code. 
- Each test should cover all cases. The name return should be written at the top, so that people know at a glance the effect to be realized.
- We discussed the features of TDD.After experiencing the TDD exercise, we discussed the advantages and disadvantages of TDD by group.
- We practiced TDD with the fizzbuzz game and Mars Rover as examples, we drew context map and discussed how many cases there should be.
## R
I am very happy to learn TDD, I think this approach is effective and gives a sense of security.
## I
- Refactoring optimizes the implemented code, makes the design simpler, and checks that variable naming can convey the meaning accurately.
- When practicing, I found that the design process became simpler. When writing test cases, you only need to consider inputs and outputs, not the development implementation.
- TDD provides quick feedback so that developers can find errors and make adjustments in a timely manner.
## D
I should follow TDD to develop my application in the future.